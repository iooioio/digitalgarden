#!/usr/bin/env bash

set -euo pipefail

SCRIPT_DIR="$(dirname "$0")"
SOURCE_DIR="$SCRIPT_DIR/src"
OUTPUT_DIR="$SCRIPT_DIR/out"


## Functions #################################################################

function escape_for_sed {
  local old_ifs="$IFS"
  local indent_str="    "
  local line

  # Ensure spaces for indentation are kept intact.
  IFS="\n"
  while read line; do
    if [ -z "$line" ]; then
      # Avoid indentation of empty lines.
      printf "\\\\n"
    else
      printf "${indent_str}${line}\\\\n"
    fi
  done

  IFS="$old_ifs"
}

function generate_page_list {
  local file
  local href
  local title

  echo "<ul>"
  for file in $(find "$SOURCE_DIR" -type f | sort); do
    href="$(echo "$file" | sed -e "s|$SOURCE_DIR/pages||" -e "s|$SOURCE_DIR||")"
    title=$(get_title "$file")

    if [ "$href" != "/index.html" -a "$href" != "/template.html" ]; then
      echo "  <li><a href=\"$href\">$title</a></li>"
    fi
  done
  echo "</ul>"
}

function get_title {
  local file="$1"

  cat "$file" | grep -oP '(?<=<h1>)[^<]+'
}

function generate_and_write_doc {
  local source_file_path="$1"
  local output_file_path="$2"
  local template_path="$3"

  local title="$(get_title "$source_file_path")"

  cat "$template_path" \
    | sed -e "s|    <!-- CONTENT -->|$(cat "$source_file_path" | escape_for_sed)|" \
          -e "s|<!-- DOC_TITLE -->|$title|" \
    > "$output_file_path"
}

function generate_and_write_docs {
  local source_dir="$1"
  local output_dir="$2"
  local template_path="$3"

  local file_name

  mkdir -p "$output_dir"

  for file_name in $(find "$source_dir" -maxdepth 1 -type f); do
    file_name="$(basename "$file_name")"
    generate_and_write_doc "$source_dir/$file_name" "$output_dir/$file_name" "$template_path"
  done
}

function generate_and_write_index {
  generate_and_write_doc "$SOURCE_DIR/index.html" "$OUTPUT_DIR/index.html" "$SOURCE_DIR/template.html"
  sed -i "s|    <!-- PAGES -->|$(generate_page_list | escape_for_sed)|" "$OUTPUT_DIR/index.html"

  # Hack to remove the prefix from the title:
  sed -i "s|<title>.*</title>|<title>$(get_title "$SOURCE_DIR/index.html")</title>|" "$OUTPUT_DIR/index.html"
}


## Entry Point ###############################################################

mkdir -p "$OUTPUT_DIR"
rm -rf "$OUTPUT_DIR"/*

generate_and_write_index
generate_and_write_docs "$SOURCE_DIR/pages" "$OUTPUT_DIR" "$SOURCE_DIR/template.html"
generate_and_write_docs "$SOURCE_DIR/posts" "$OUTPUT_DIR/posts" "$SOURCE_DIR/template.html"

